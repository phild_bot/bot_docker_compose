# bot_docker_compose

Docker compose files for bot. The images are created from [bot_docker](https://gitlab.com/phild_bot/bot_docker).

- [bot_base](bot_base/docker-compose.yml): Deploys a docker container for  [bot_base](https://gitlab.com/phild_bot/bot_base).
- [bot_ui](bot_ui/docker-compose.yml): Deploys a docker container for [bot_ui](https://gitlab.com/phild_bot/bot_ui).
